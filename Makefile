WEB_EXT=./node_modules/web-ext/bin/web-ext.js

lint:
	${WEB_EXT} --source-dir src lint

src/icons/gts.svg: src/icons/icon.svg
	svgcleaner src/icons/icon.svg src/icons/gts.svg

pack: src/icons/gts.svg
	${WEB_EXT} --source-dir src build

run:
	${WEB_EXT} --source-dir src run

install:
	npm install --save-dev web-ext

clean:
	rm -rf package-lock.json node_modules/

devserver:
	python -m http.server
