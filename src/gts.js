window.addEventListener('hashchange', colorise_inbox);
window.addEventListener('popstate', colorise_inbox);
colorise_inbox();

//* <- enable when building the extension
if (browser && browser.browserAction && browser.browserAction.onClicked && browser.browserAction.onClicked.addListener) {
    console.log("browserAction registered");
    browser.browserAction.onClicked.addListener(async () => {
        console.log("gts.js clicked");
        let activeTab = await browser.tabs.query({active: true, currentWindow: true});
        console.log(["activeTab", activeTab]);
        colorise_inbox();
    });
}
// */

function colorise_inbox() {
//* <- dev switch
if (location.hostname !== "mail.google.com"
    || location.pathname !== '/mail/u/0/'
    || location.hash !== "#inbox") {
    return 0;
}
setTimeout(_ => requestAnimationFrame(colorise_inbox), 42*1000);
// */
console.log("gts.js colorising ...");

const ONE_DAY = 24 * 60 * 60 * 1000;

let mail_table = find_mail_table();
let rows = mail_table.querySelectorAll('tr');
let now = Date.now();

const COLOURS = [
    // light green
    'rgba(100, 255, 100, 0.8)',
    // light yellow
    'rgba(255, 255, 100, 0.7)',
    // light blue
    'rgba(164, 229, 255, 0.6)',
    // light orange
    'rgba(255, 107, 100, 0.3)',
];

let last_24h = ONE_DAY;
let yesterday = 2 * ONE_DAY;
// falses after yesterday will get added as needed
let firsts = [ false, false, ];

for (let i = 0; i < rows.length; ++i) {
    let tr = rows[i];
    let time = find_time_in_row(tr);
    let delta = now - time;
    let n_days_back = Math.floor( delta / ONE_DAY );
    
    if (typeof COLOURS[n_days_back] === 'undefined') {
        // maybe reset the colour
        tr.style.backgroundColor = '';
    } else {
        tr.style.backgroundColor = COLOURS[n_days_back];
        if (firsts[n_days_back] !== true) {
            firsts[n_days_back] = true;
            if (n_days_back !== 0) {
                tr.querySelectorAll('td').forEach(e => e.style.paddingTop = "2em");
            }
        }
    }
}

// insert a break row if nothing left to do today
if (firsts[0] == false && rows.length > 0) {
    let tds = rows[0].querySelectorAll('td');
    let new_row = document.createElement('tr');
    for (let i = 0; i < tds.length; ++i) {
        let td = tds[i];
        let new_td = document.createElement('td');
        new_td.style.backgroundColor = COLOURS[0];
        if (td.getAttribute('role') === "gridcell" ) { 
            new_td.innerHTML = '<div class="xS">Nothing to do today 🎉</div>';
        }
        new_row.appendChild(new_td);
    }
    
    let tbody = mail_table.querySelector('tbody');
    tbody.insertBefore(new_row, tbody.children[0]); 
}

function find_time_in_row(tr) {
    let spans = tr.querySelectorAll('span');
    for (let i = 0; i < spans.length; ++i) {
        let span = spans[i];
        if (typeof span.title === 'string' && span.title.trim() !== '') {
            let fragments = span.title.split(",");
            if (fragments.length !== 3) {
                console.error(["invalid .title", span, fragments]);
                throw "invalid .title";
            }
            let date = fragments[1].split('.').join('')
            let dt = Date.parse(`${date} ${fragments[2]}`);
            return dt;
        }
    }
}

function find_mail_table() {
     let tables = document.querySelectorAll('table');
     for (let i = 0; i < tables.length; ++i) {
         let table = tables[i];
         if (table.id.startsWith(':')) {
             return table;
         }
     }
     throw "unable to find mail table";
}

// end of function colorise_inbox
}

